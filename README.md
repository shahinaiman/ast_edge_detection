# Image edge detection using c++

This repository has a completely functional program that takes a dataset of images 
and process them to get an image highlighting the edges in it.
later these images can be used for AI training.

to read about the project you can refer to the documentation in the repository [here](project_documents/Protocol_MBE_Fozeu_Shahin.pdf). this document also 
to read about the project you can refer to the documentation in the repository [here](project_documents/Protocol_MBE_Fozeu_Shahin.pdf). this document also 
has a tutorial to setup OpenCV for visual studio.

This is a project for the Advanced Software Techniques Module in the
Berliner Hochschule für Technick.

edge detected images can be found [here](Bildverarbeitung/edge_detected_image/).

#### if you want to run the program quickly
- go to Bildverarbeitung\x64\Debug directory
- there you will find a folder called dataset.
- open this folder and place all the images in the folder preferably with .png extension.
- in the same Bildverarbeitung\x64\Debug directory you will find Bildverarbeitung.exe
- run it.
- you will find the output of the program in edge_detected_image directory.

The project has certain metrics that needs to be covered

### Git
- [x] 1-  Use and understand Git!             

to get a copy of the code please type the following
```
git clone https://gitlab.com/shahinaiman/ast_edge_detection.git
```

### UML
- [x] 2- UML                                 
currently there is 4 UML diagram created

a simple Activity Diagram showing the flow of activity [here](project_documents/activity_diagram.drawio.png).

a Use Case diagram showing some actors and showing responsibilties [here](project_documents/Use_case_Diagram.png).

a Current Component diagram showing the component of the product [here](project_documents/current_component_diagram.png).

a Future Component diagram when investment recieved [here](project_documents/future_component_diagram.png).

### DDD
- [x] 3- DDD                               
in this part, I have visualized how the application should be like in the future [here](project_documents/domain.png). 
it also has some components that are already existing and some can be implemented in the future

### Metrics
- [x] 4- Metrics                             
since sonarqube for c++ is not free I had to look into other tools. 
luckily I found this tool which has a free trail for the next 7 days from 11.02.2023.

from https://app.codacy.com

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/35aec62d4b41462dba874ef664896a6e)](https://www.codacy.com/gl/shahinaiman/ast_edge_detection/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=shahinaiman/ast_edge_detection&amp;utm_campaign=Badge_Grade)

these are the metrics that they report:
- issues in code (bad practice or non standard syntax etc.)
- complexity
- Duplication

![metrics](doc_images/metrics.png)

### Clean Code development
- [x] 5- Clean Code Development.

two documents was followed to write in clean code
please refer to the document [here](project_documents/Clean_code.pdf)

### Build Management
- [x] 6- Build Management
For a Visual Studio C++ project using OpenCV, I used the Visual Studio built-in build system to manage my build. 

Here's how I set it up:

- Open your project in Visual Studio.
- Right-click on your project in the Solution Explorer and select "Properties".
- Go to Configuration Properties > VC++ Directories.
- Add the path to your OpenCV include directory to the "Include Directories" field.
- Add the path to your OpenCV library directory to the "Library Directories" field.
- Go to Configuration Properties > Linker > Input.
- Add opencv_world454.lib to the "Additional Dependencies" field.

![build1](doc_images/build1.png)
![build2](doc_images/build2.png)

you can find a step by step guide [here](project_documents/Protocol_MBE_Fozeu_Shahin.pdf) at the end with pictures for setting up the opencv libraries

just for learning I created a CMakeLists.txt [here](CMakeLists.txt). however I am not using it as I am already satisfied with visual studio build.

the following picture is from a different project where I ported nfc reader of stm into esp32 micro controller

![build3](doc_images/build3.png)

the esp idf has its own tools for cmake which makes it easier to build the project

### Unit tests
- [x] 7- Unit-Tests.

I have created three test cases to make sure the 
program runs in a desired manner.
test1 

![test1](doc_images/test1.JPG)

test2

![test2](doc_images/test2.JPG)

test3 

![test3](doc_images/test3.JPG)

success of unit test

![unit_tests_success](doc_images/unittest.png)

Unit test run on jenkinis

![jenkins_unit_tests_success](doc_images/jenkinsunittest.png)

### Continous Delivery
- [x] 8- Continuous Delivery

I have setup a jenkins instance. I explored how to do it using AWS which was interesting. but I choose to continue using normal method
after setting up jenkins and downloading important plugins, I created aa Freestyle project.
in order to integrate the tools with gitlab I had to get access token from gitlab to provide it to jenkins. and vice versa.

after experimenting I decided to 
- compile the code using MSBuild since my code is from visual studio
- run unit test on jenkins using vstest.console.exe and powershell
- run DSL file
- run the executable file
- finally post-build I archived the project files

successful build is archieved
![artifact](doc_images/jenkins1.png)

Solution is Built using MSbuild
![MSbuild](doc_images/jenkins2.png)

Unit test success using jenkins
![jenkins_unit_tests_success](doc_images/jenkinsunittest.png)

configuring Unit test run command
![jenkins_unit_tests_config](doc_images/jenkinsunittest2.png)

commands to run DSL and project
![batch command](doc_images/jenkins3.png)

achrive commands
![archive success build](doc_images/jenkins4.png)

success message
![success message](doc_images/jenkins5.png)


### Understanding IDE
- [x] 9- Understanding IDE                
I have choosen Visual Studio IDE because it is the best to integrate opencv with.
additionally I like the interface. however my favourite is clion from jet brains.

I use crtl + f alot and also crtl + r

### DSL
- [x] 10- DSL
I created a DSL that calculates the size of bytes of total images in the orginal images folder
and the edge procesed folder. in addition it counts the number of images in the original folder and 
the number of images processed.
here is a picture of the output of the DSL

![dsl_output](doc_images/DSL_output.png)

also here is the code

![dsl_code](doc_images/DSL_code.png)

### Functional Programming
- [x] 11- Functional Programming

When using the program you will find that each step and process is in a seperate file and in a seperate function.
dividing the steps so it would not become a spagettii code.
additionally each step the image processed is stored in a folder relative to the type of process;
for exampe filtered_image folder which holds the filtered images.
another example is edge_detected_image folder which holds the final processed image 
