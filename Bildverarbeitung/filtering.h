//#pragma once
#include <opencv2/opencv.hpp> //used to navigate through picture pixels and evaluate it or change it
//#include <iostream> 
//#include <random> 
//#include <ctime>
//#include <vector>
//#include <cmath> 

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
using namespace cv;
using namespace std;

#define M_PI 3.14159265358979323846

/// <summary>
/// this function filter and smooth the picture through convolution
/// it can be used for different types of kernels
/// </summary>
/// <param name="Mask"></param> the filter mask
/// <param name="image"></param> the original image
/// <param name="Filter"></param> the filtered image
int convolution_2D_filtering(Mat& Mask, Mat& image, Mat& Filter);


/// <summary>
/// Gaussian Filtering creation (1/(2*PI*sigma^2))*e^(-((x^2+y^2)/2*sigma^2))
/// This function takes the Mask matrix and fill it with Gaussian filter values
/// 
/// </summary>
/// <param name="Mask"></param>
/// <param name="s"></param>
void gaussian_filter(Mat& Mask, float s);
