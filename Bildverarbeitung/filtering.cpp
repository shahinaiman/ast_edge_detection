#include "main.h"
#include "filtering.h"

using namespace cv;
using namespace std;
namespace fs = std::filesystem;

/// this function filter and smooth the picture through convolution
/// it can be used for different types of kernels
int convolution_2D_filtering(Mat& Mask, Mat& image, Mat& Filter)
{
    Mat neu;
    float inv = 1.0;
    int x_pos = 0;                  //x corrdinate of pixel
    int y_pos = 0;                  //y corrdinate of pixel
    float sum_of_elements = 0;
    // checks that the filter size is correct
    if (Mask.rows == 3 && Mask.cols == 3)
    {
        neu = Mat_<float>(image.rows + 2, image.cols + 2);
        Filter = Mat_<float>(neu.rows, neu.cols);
        for (int r = 0; r < neu.rows; r++)
        {
            for (int c = 0; c < neu.cols; c++)
            {
                neu.at<float>(r, c) = 0;
                Filter.at<float>(r, c) = 0;
            }
        }

        for (int r = 0; r < image.rows; r++)
        {
            for (int c = 0; c < image.cols; c++)
            {
                neu.at<float>(r + 1, c + 1) = image.at<float>(r, c);
            }
        }

        // convolution process - please refer to the document for more details
        for (int r = 0; r < image.rows; r++)
        {
            for (int c = 0; c < image.cols; c++)
            {
                for (int x = x_pos; x < Mask.rows + x_pos; x++)
                {
                    for (int y = y_pos; y < Mask.cols + y_pos; y++)
                    {
                        
                        sum_of_elements += neu.at<float>(x, y) * Mask.at<float>(x - x_pos, y - y_pos);
                    }
                }
                Filter.at<float>(r + 1, c + 1) = (inv * sum_of_elements);
                sum_of_elements = 0;
                y_pos++;
            }
            x_pos++;
            y_pos = 0;
        }
    }
    else {
        cout << "the kernal size was not 3*3" << endl;
        return -1;
    }
}

// function to change filter from normal to gaussian
// Gaussian Filtering creation(1 / (2 * PI * sigma ^ 2))* e ^ (-((x ^ 2 + y ^ 2) / 2 * sigma ^ 2))
void gaussian_filter(Mat& Mask, float s) {
    //M_PI
    int side_pad = 1;                        // padding around image for convolution
    int filter_size = 3;                       // filter size 3*3
    float sigma = s;                    // sigma
    float left_part = (1 / (2 * M_PI * sigma * sigma)); //(1/(2*PI*sigma^2)) left part of filter calculation
    float sum = 0;
    float exp_numerator;                            // -((x ^ 2 + y ^ 2)  in e^(-((x^2+y^2)/2*sigma^2))
    float exp_denominator = (2 * sigma * sigma); // 2*sigma^2 in e^(-((x^2+y^2)/2*sigma^2))

    //looping through the matrix element by element
    for (int col = -side_pad; col <= side_pad; col++) {

        for (int row = -side_pad; row <= side_pad; row++) {
            exp_numerator = (row * row + col * col); // (x^2+y^2) in e^(-((x^2+y^2)/2*sigma^2))
            // left part          * e^ -numerator/denominator   
            // (1/(2*PI*sigma^2)) *  e^(-((x^2+y^2)/2*sigma^2))
            Mask.at<float>(row + side_pad, col + side_pad) = left_part * exp(-exp_numerator / exp_denominator);
            sum += Mask.at<float>(row + side_pad, col + side_pad); // later will be used to divide all the elements by it
        }
    }
    //loop through each element in the matrix to divided b� sum which was evaluated before
    for (int col = 0; col < filter_size; col++) {
        for (int row = 0; row < filter_size; row++) {
            Mask.at<float>(row, col) /= sum;
        }
    }
}
