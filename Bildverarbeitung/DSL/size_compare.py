import os

def sum_image_sizes(folder1, folder2):

    image_extensions = ('jfif','.jpg', '.png', '.gif', '.bmp', '.jpeg')
    sizes1 = [os.path.getsize(os.path.join(folder1, f)) for f in os.listdir(folder1) if f.endswith(image_extensions)]
    sizes2 = [os.path.getsize(os.path.join(folder2, f)) for f in os.listdir(folder2) if f.endswith(image_extensions)]
    size1 = sum(sizes1)
    size2 = sum(sizes2)
    count1 = len(sizes1)
    count2 = len(sizes2)
    return size1, size2, count1, count2

if __name__ == '__main__':

    #folder1 = input("Enter the path of the first folder: ")
    #folder2 = input("Enter the path of the second folder: ")
    folder2="../edge_detected_image"
    folder1="../dataset"
    size1, size2, count1, count2 = sum_image_sizes(folder1, folder2)
    print("The total size of all orginal images is: ", size1, "bytes")
    print("The number of images existing in the folder is: ", count1)
    print("The total size of all images processed is: ", size2, "bytes")
    print("The number of images processed: ", count2)
