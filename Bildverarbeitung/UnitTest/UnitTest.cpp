#include "pch.h"
#include "CppUnitTest.h"


using namespace cv;
using namespace std;
namespace fs = std::filesystem;

// Global Variables ////

extern Mat_<float>gradient_opx;      // gradient operator x direction. we will use sobel.
extern Mat_<float> gradient_opy;     // gradient operator y direction. we will use sobel.
extern Mat_<float> Mask;             // kernel holding values to smooth and filter the image.


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	TEST_CLASS(UnitTest)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			vector<string> images_paths;
			Assert::IsFalse(get_image("non-existent-dir/", &images_paths));
			Assert::IsFalse(get_image("testimages/", &images_paths));
		}
		TEST_METHOD(TestMethod2) {
			Mat image;                  // image pixel saved here
			int count = 0;              // count of images
			int error_ED;               // error returned here

			vector<string> images_paths;
			get_image("non-existent-dir/", &images_paths);
			
			vector<string>::iterator it;
			for (it = images_paths.begin(); it != images_paths.end(); it++) {

				count++;

				image = imread(*it, IMREAD_GRAYSCALE);
				// start of image processing
				Assert::AreEqual(ed_process(image, count),-1);
				
			}
		}
		TEST_METHOD(TestMethod3) {
			Mat image;                  // image pixel saved here
			int count = 0;              // count of images
			int error_ED;               // error returned here

			vector<string> images_paths;
			get_image("testimages/", &images_paths);

			vector<string>::iterator it;
			for (it = images_paths.begin(); it != images_paths.end(); it++) {

				count++;

				image = imread(*it, IMREAD_GRAYSCALE);
				// start of image processing
				Mask << 1, 1, 1;
				Assert::AreEqual(ed_process(image, count), -2);
			}
		}
	};
}
