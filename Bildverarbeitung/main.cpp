
#include "main.h" 
#include "service.h"


using namespace cv;
using namespace std;
namespace fs = std::filesystem;

#define DS_directory "dataset/"
//string path("../dataset2/street_data/");

int main(int argc, char** argv)
{
    
    vector<string> images_paths;
    set_paramters();
    
    if (get_image(DS_directory, &images_paths)) {
        std::cout << "images paths was loaded successfully" << std::endl;
    }
    else {
        std::cout << "Program failed due to missing paths" << std::endl;
    }

    if (loop_images(images_paths)) {
        std::cout << "images was processed successfully" << std::endl;
    }
    else {
        std::cout << "Program failed" << std::endl;
    }
    
    waitKey(0);
    return 0;
}