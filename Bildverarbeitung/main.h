#pragma once
#include <opencv2/opencv.hpp> //used to navigate through picture pixels and evaluate it or change it
#include <iostream> 
#include <random> 
#include <ctime>
#include <vector>
#include <cmath> 

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <fstream>
#include <filesystem>
#include <string>

#include "filtering.h"
#include "gradient_calculation.h"



#ifndef debug_mode
//#define debug_mode
#endif