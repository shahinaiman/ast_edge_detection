#include "main.h"
#include "gradient_calculation.h"

using namespace cv;
using namespace std;
namespace fs = std::filesystem;

void gradient_intensity_and_angle(Mat& sobel_x_pixel, Mat& sobel_y_pixel, Mat& grad_intensity, Mat& grad_angle,int count)
{

    grad_intensity = Mat_<float>(sobel_x_pixel.rows, sobel_x_pixel.cols);  // gradient intensity
    grad_angle = Mat_<float>(sobel_x_pixel.rows, sobel_x_pixel.cols); // gradient angle

    string s_count = to_string(count);   // count of image
    string save_img_path = "gradient_image/Gradient" + s_count + ".png"; //path of image

    // calculate the intensity and the angle of each pixel
    // angles then are filtered between 0, 45, 90 and 135
    for (int r = 0; r < sobel_x_pixel.rows; r++)
    {
        for (int c = 0; c < sobel_x_pixel.cols; c++)
        {
            // square root(x^2+y^2)
            grad_intensity.at<float>(r, c) = sqrtf(pow(sobel_x_pixel.at<float>(r, c), 2) + pow(sobel_y_pixel.at<float>(r, c), 2));
            //tan^-1(-x/y) to get angle
            grad_angle.at<float>(r, c) = float(atan2f(-(sobel_y_pixel.at<float>(r, c)), sobel_x_pixel.at<float>(r, c))) * 180.0 / 3.141592;
            // 0 for angles between -22.5 and 22.5 also bigger than 157.5 and smaller than -157.5
            if (((grad_angle.at<float>(r, c) < 22.5) && (grad_angle.at<float>(r, c) > -22.5)) || (grad_angle.at<float>(r, c) > 157.5) || (grad_angle.at<float>(r, c) < -157.5))
            {
                grad_angle.at<float>(r, c) = 0;
            }
            // 45 for angles between 22.5 and 67, 5 also bigger than - 157.5 and smaller than - 112.5
            else if (((grad_angle.at<float>(r, c) > 22.5) && (grad_angle.at<float>(r, c) < 67.5)) || ((grad_angle.at<float>(r, c) < -112.5) && (grad_angle.at<float>(r, c) > -157.5)))
            {
                grad_angle.at<float>(r, c) = 45;
            }
            // 90 for angles between 67.5 and 112.5 also bigger than -112.5 and smaller than -67.5
            else if (((grad_angle.at<float>(r, c) > 67.5) && (grad_angle.at<float>(r, c) < 112.5)) || ((grad_angle.at<float>(r, c) < -67.5) && (grad_angle.at<float>(r, c) > -112.5)))
            {
                grad_angle.at<float>(r, c) = 90;
            }
            // 135 for angles between 112.5 and 157.5 also bigger than -67.5 and smaller than -22.5
            else if (((grad_angle.at<float>(r, c) > 112.5) && (grad_angle.at<float>(r, c) < 157.5)) || ((grad_angle.at<float>(r, c) < -22.5) && (grad_angle.at<float>(r, c) > -67.5)))
            {
                grad_angle.at<float>(r, c) = 135;
            }
        }
    }
    
    imwrite(save_img_path, grad_intensity);
    // print image for debugging
#ifdef debug_mode
    Mat gradient_img = imread(save_img_path);
    imshow("Gradientxy", gradient_img);
#endif
}

// Removing the non-maximums is a fairly quick step. 
// The idea is to remove from the gradient norm all the "weak" values
// please refer to the document in the repository for more information
void new_gradient(Mat& grad_intensity, Mat& grad_angle) {
    for (int r = 0; r < grad_intensity.rows; r++)
    {
        for (int c = 0; c < grad_intensity.cols; c++)
        {
            if (grad_intensity.at<float>(r, c) == 0 || grad_intensity.at<float>(r, c) == -0)
            {
                continue;
            }
            else {
                //x +1 and x-1
                if (grad_angle.at<float>(r, c) == 0) {
                    if (grad_angle.at<float>(r, c) < grad_angle.at<float>(r + 1, c) || grad_angle.at<float>(r, c) < grad_angle.at<float>(r - 1, c)) {
                        grad_intensity.at<float>(r, c) = 0;
                    }
                }//x+1 y+1 and  x-1 y-1
                else if (grad_angle.at<float>(r, c) == 45) {
                    if (grad_angle.at<float>(r, c) < grad_angle.at<float>(r + 1, c + 1) || grad_angle.at<float>(r, c) < grad_angle.at<float>(r - 1, c - 1)) {
                        grad_intensity.at<float>(r, c) = 0;
                    }
                }//y+1  and  y-1
                else if (grad_angle.at<float>(r, c) == 90) {
                    if (grad_angle.at<float>(r, c) < grad_angle.at<float>(r, c + 1) || grad_angle.at<float>(r, c) < grad_angle.at<float>(r, c - 1)) {
                        grad_intensity.at<float>(r, c) = 0;
                    }
                }//x-1 y+1  and x+1 y-1
                else if (grad_angle.at<float>(r, c) == 135) {
                    if (grad_angle.at<float>(r, c) < grad_angle.at<float>(r - 1, c + 1) || grad_angle.at<float>(r, c) < grad_angle.at<float>(r + 1, c - 1)) {
                        grad_intensity.at<float>(r, c) = 0;
                    }
                }
            }
        }
    }
}

void edge_detection(Mat& grad_intensity, Mat& grad_angle) {
    //high threshold ist  40 and low thresholf is 0.5*40
    int TH = 120;  // high threshold
    int TL = 60;   // low threshold
    float angle;

    // pass through all pixels
    // if pixel intensity above high threshold pixel value becomes 255
    // else if pixel value less than low threshold pixel value becomes 0
    for (int r = 0; r < grad_intensity.rows; r++)
    {
        for (int c = 0; c < grad_intensity.cols; c++)
        {
            if (grad_intensity.at<float>(r, c) >= TH) {
                grad_intensity.at<float>(r, c) = 255;
            }
            else if (grad_intensity.at<float>(r, c) <= TL) {
                grad_intensity.at<float>(r, c) = 0;
            }
        }
    }

    // next step is to check for all pixels that are in between
    // check the neighbouring values of the current pixel depending on the angle value
    // of the pixel
    for (int r = 0; r < grad_intensity.rows; r++)
    {
        for (int c = 0; c < grad_intensity.cols; c++)
        {
            if (grad_intensity.at<float>(r, c) > TL && grad_intensity.at<float>(r, c) < TH) {
                angle = grad_angle.at<float>(r, c) + 90;

                //if angle = 90 check neighbour pixel intensity y+1 y-1
                if (angle == 90) {
                    if (grad_angle.at<float>(r, c + 1) > TH || grad_angle.at<float>(r, c - 1) > TH) {
                        grad_intensity.at<float>(r, c) = 255;
                    }
                    else if (grad_angle.at<float>(r, c + 1) < TL || grad_angle.at<float>(r, c - 1) < TL) {
                        grad_intensity.at<float>(r, c) = 0;
                    }
                }//if angle = 135 check both neighbour pixel intensity x-1 y+1  and x+1 y-1
                else if (angle == 135) {
                    if (grad_angle.at<float>(r - 1, c + 1) > TH || grad_angle.at<float>(r + 1, c - 1) > TH) {
                        grad_intensity.at<float>(r, c) = 255;
                    }
                    else if (grad_angle.at<float>(r - 1, c + 1) < TL || grad_angle.at<float>(r + 1, c - 1) < TL) {
                        grad_intensity.at<float>(r, c) = 0;
                    }
                }// if angle = 180 check neighbour pixel intensity x-1 y and x+1 y
                else if (angle == 180) {
                    if (grad_angle.at<float>(r - 1, c) > TH || grad_angle.at<float>(r + 1, c) > TH) {
                        grad_intensity.at<float>(r, c) = 255;
                    }
                    else if (grad_angle.at<float>(r - 1, c) < TL || grad_angle.at<float>(r + 1, c) < TL) {
                        grad_intensity.at<float>(r, c) = 0;
                    }
                }// if angle = 90 check neighbour pixel intensity x-1 y-1 x+1 y+1
                else if (angle == 225) {
                    if (grad_angle.at<float>(r + 1, c + 1) > TH || grad_angle.at<float>(r - 1, c - 1) > TH) {
                        grad_intensity.at<float>(r, c) = 255;
                    }
                    else if (grad_angle.at<float>(r + 1, c + 1) < TL || grad_angle.at<float>(r - 1, c - 1) < TL) {
                        grad_intensity.at<float>(r, c) = 0;
                    }
                }
            }
        }
    }
}



void gradient( Mat& gradient_x, Mat& gradient_y, Mat& grad_intensity, Mat& grad_angle,int count)
{   
#ifdef debug_mode
    cout << "gradient X direction size = " << endl << " " << Fil_x.size() << endl << endl;
    cout << "gradient Y direction size = " << endl << " " << Fil_y.size() << endl << endl;
#endif

    // calculating the gradient intensity and angle
    gradient_intensity_and_angle(gradient_x, gradient_y, grad_intensity, grad_angle, count);
    // passing the intensity and angle to get the edges of image
    new_gradient(grad_intensity, grad_angle);
    // thresholding and getting the final result
    edge_detection(grad_intensity, grad_angle);
}