#pragma once
#include "main.h"

/// <summary>
/// initialize paramters used for convolution and gradient operation
/// </summary>
void set_paramters();

/// <summary>
/// get the paths for the images from directory
/// </summary>
/// <param name="dir"></param>
/// <param name="images_paths"></param>
/// <returns></returns>
bool get_image(string dir, vector<string>* images_paths);

/// <summary>
/// loop through the images paths and process them
/// </summary>
/// <param name="images_paths"></param>
/// <returns></returns>
bool loop_images(vector<string> images_paths);

/// <summary>
/// process for edge detection
/// </summary>
/// <param name="image"></param>
/// <param name="count"></param>
/// <returns></returns>
int ed_process(Mat& image, int count);


/// <summary>
/// Convert a 1-channel Mat<float> object to a vector.
/// </summary>
/// <param name="in"></param>
/// <param name="out"></param>
/// <returns></returns>
void mat_to_vector(const Mat& in, vector<float>& out);