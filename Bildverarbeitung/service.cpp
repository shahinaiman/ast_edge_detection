#include "service.h"

using namespace cv;
using namespace std;
namespace fs = std::filesystem;

// Global Variables ////

Mat_<float>gradient_opx(3, 3);      // gradient operator x direction. we will use sobel.
Mat_<float> gradient_opy(3, 3);     // gradient operator y direction. we will use sobel.
Mat_<float> Mask(3, 3);             // kernel holding values to smooth and filter the image.

void set_paramters() {
    // Mask for smoothing and filtering image
    // this is the intial mask that will be changed to a Gaussian filter
    Mask << 1, 1, 1,
        1, 1, 1,
        1, 1, 1;

    //function to adjust the mask to gaussian filter
    gaussian_filter(Mask, 1.2);
    ///////////////////////////////////////////////////////////////////////


    // sobel operator used to calculate the gradient angle and intensity
    // edit this if you want to use a different operator
    gradient_opx << -1, 0, 1,
        -2, 0, 2,
        -1, 0, 1;

    gradient_opy << 1, 2, 1,
        0, 0, 0,
        -1, -2, -1;
    ////////////////////////////////////////////////////////////////////
        // printing for debugging purposes
#ifdef debug_mode 
    cout << "Mask_Size = " << endl << " " << Mask << endl << endl;
#endif
}

bool loop_images(vector<string> images_paths) {

    Mat image;                  // image pixel saved here
    int count = 0;              // count of images
    int error_ED;               // error returned here

    vector<string>::iterator it;
    for (it = images_paths.begin(); it != images_paths.end(); it++) {

        count++;

        image = imread(*it, IMREAD_GRAYSCALE);
        // start �f image processing
        error_ED = ed_process(image, count);

        if (error_ED == -1) {
            cout << "programm failed due to wrong image path" << endl;
            return false;
        }
        else if (error_ED == -2) {
            cout << "programm failed due to wrong kernal" << endl;
            return false;
        }
    }
    cout << count << " is the total number of images processed" << endl;

    return true;

}

// the flow of edge detection process.
// first import image
// second smooth and filter image
// third get the x and y sobel images
// fourth get gradient intensity and angle (in gradient function)
// fifth remove non maximums (in gradient function)
// sixth thresholding by hysteresis and getting the final result (in gradient function)
int ed_process(Mat& image, int count) {

    string s_count = to_string(count);  // holds count of image
    Mat filtered_img;                   // holds smoothed and filtered image
    Mat sobel_filter_x;                 // holds x direction sobel image
    Mat sobel_filter_y;                 // holds y direction sobel image
    Mat grad;                           // holds intensity of each pixel
    Mat gradAngle;                      // holds angle of each pixel
    int error_ED;                       // error returned here
    ////////////////////////////////////////////////////////////////////////
    // assert image exist
    // Check for failure and report error
    if (image.empty())
    {
        cout << "Image Not Found!!!" << endl;
        cin.get(); //wait for any key press
        return -1;
    }
    // store grayscale image
    // create a copy of original image and save it in folder
    string save_img_path = "image_copy/Original_Image" + s_count + ".png";
    imwrite(save_img_path, image);

    // Show our image inside a window.
    // used for debugging purposes
#ifdef debug_mode 
    imshow("Original image", image);
#endif

    //////////////////////////////////////////////////////////////////////////

    vector<float>array;
    image.convertTo(image, CV_32F);
#ifdef debug_mode
    cout << "Image_Size = " << endl << " " << image.size() << endl << endl;
#endif

    // smoothing and filtering image by convolution

    error_ED = convolution_2D_filtering(Mask, image, filtered_img);
    if (error_ED == -1) {
        cout << "kernal size error" << endl;
        return -2;
    }
#ifdef debug_mode
    cout << "Size_Image_filtered = " << endl << " " << filter.size() << endl << endl;
#endif

    save_img_path = "filtered_image/f_image" + s_count + ".png";
    mat_to_vector(filtered_img, array);
    Mat filtered_image = Mat(filtered_img.rows, filtered_img.cols, filtered_img.type(), array.data());
    imwrite(save_img_path, filtered_image);

    // show image for debugging
#ifdef debug_mode 
    Mat load_filtered_image = imread(save_img_path);
    imshow("filtered_image", load_filtered_image);
#endif
    filtered_img.convertTo(filtered_img, CV_32F);
    /////////////////////////////////////////////////////////////////////////////////

    // convolution of filtered image using sobel operator x direction
    save_img_path = "sobel_x/Sobel_x" + s_count + ".png";
    error_ED = convolution_2D_filtering(gradient_opx, filtered_img, sobel_filter_x);
    if (error_ED == -1) {
        cout << "kernal size error" << endl;
        return -2;
    }
    //cout << " Filter_x = " << endl << " " << filter_x << endl << endl;
    imwrite(save_img_path, sobel_filter_x);
    // debugging to see image
#ifdef debug_mode 
    Mat test0 = imread(save_img_path);
    imshow("Sobel_x", test0);
#endif

    save_img_path = "sobel_y/Sobel_y" + s_count + ".png";
    // convolution of filtered image using sobel operator y direction
    error_ED = convolution_2D_filtering(gradient_opy, filtered_img, sobel_filter_y);
    if (error_ED == -1) {
        cout << "kernal size error" << endl;
        return -2;
    }
    imwrite(save_img_path, sobel_filter_y);
    // debugging
#ifdef debug_mode 
    Mat test10 = imread(save_img_path);
    imshow("Sobel_y", test10);
#endif

    /////////////////////////////////////////////////////////////////////////////////

    save_img_path = "edge_detected_image/Image_Final" + s_count + ".png";
    // passing operators and filterd sobel images to get gradient intensity and angle
    gradient(sobel_filter_x, sobel_filter_y, grad, gradAngle, count);
    //cout << "Gradient = " << endl << " " << grad << endl << endl;
    //cout << "Angle_gradient = " << endl << " " << gradAngle << endl << endl;
    imwrite(save_img_path, grad);
    // debugging
#ifdef debug_mode 
    Mat ED_final_image = imread(save_img_path);
    imshow("edge detection result", ED_final_image);
#endif
    return 1;
}

bool get_image(string dir, vector<string>* images_paths) {

    vector<string> images_path;

    Mat image;                  // image pixel saved here
    string path(dir);           // path to directory


    string ext(".png");         //extensions allowed
    string ext2(".jpg");        //extensions allowed
    string ext3(".jfif");       //extensions allowed

    int error_ED;               // error returned here
    try {
        // looping through all files in directory
        for (auto& p : fs::recursive_directory_iterator(path))
        {

            if (p.path().extension() == ext || p.path().extension() == ext2 || p.path().extension() == ext3) {
                // print image name for debugging
                images_path.push_back(p.path().string());
                // image = imread(path.c_str() + p.path().stem().string() + p.path().extension().string(), IMREAD_GRAYSCALE);
#ifdef debug_mode
                cout << p.path().stem().string() << '\n';
#endif
            }
            else {
                return false;
            }
        }

        *images_paths = images_path;

        return true;
    }
    catch (...) {
        return false;
    }

    
}




// Convert a 1-channel Mat<float> object to a vector. 
void mat_to_vector(const Mat& in, vector<float>& out)
{
    if (in.isContinuous())
    {
        out.assign((float*)in.datastart, (float*)in.dataend);
    }
    else
    {
        for (int i = 0; i < in.rows; ++i)
        {

            out.insert(out.end(), in.ptr<float>(i), in.ptr<float>(i) + in.cols);

        }
    }
    return;
}

