//#pragma once

#include <opencv2/opencv.hpp> //used to navigate through picture pixels and evaluate it or change it
#include <iostream> 
#include <random> 
#include <ctime>
#include <vector>
#include <cmath> 

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
using namespace std;

/// <summary>
/// we use the Sobel operator to calculate the gradient 
/// in the x-direction and y-direction.
/// </summary>
/// <param name="sobel_x_pixel"></param>
/// <param name="sobel_y_pixel"></param>
/// <param name="grad_intensity"></param>
/// <param name="grad_angle"></param>
void gradient_intensity_and_angle(Mat& sobel_x_pixel, Mat& sobel_y_pixel, Mat& grad_intensity, Mat& grad_angle, int count);

/// <summary>
/// Removing the non-maximums is a fairly quick step. 
/// The idea is to remove from the gradient norm all the "weak" values
/// </summary>
/// <param name="grad_intensity"></param>
/// <param name="grad_angle"></param>
void new_gradient(Mat& grad_intensity, Mat& grad_angle);

/// <summary>
/// 1. As a first step / a first loop, remove all the weak pixels lower 
/// than TL and set all the strong pixels higher than Th with a value
/// of 255.
/// 
/// 2. Once this first pass has been carried out, 
/// a second iteration will allow each pixel located 
/// between TLand Th to be processed more finely with the 
/// matrix of gradients.During this second iteration, 
/// we will get the neighbours of the pixeland make the same
/// comparison as before.Do not forget to add 90 degrees to 
/// the gradient angle of the pixel considered before recovering the
/// neighbours.
/// </summary>
/// <param name="grad_intensity"></param>
/// <param name="grad_angle"></param>
void edge_detection(Mat& grad_intensity, Mat& grad_angle);

/// <summary>
/// get x-y direction of sobel and pass grad and angle variable
/// pass x and y direction of sobel-image to get gradient intensity and direction
/// then remove non maximums before passing it to the thresholding function
/// </summary>
/// <param name="Fil_x"></param>
/// <param name="Fil_y"></param>
/// <param name="Grad"></param>
/// <param name="Angle"></param>
/// <param name="count"></param>
void gradient(Mat& gradient_x, Mat& gradient_y, Mat& grad_intensity, Mat& grad_angle, int count);
