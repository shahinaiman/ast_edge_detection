# Define the compiler and flags
CC=g++
CFLAGS=-Wall -I/usr/local/include
LDFLAGS=-L/usr/local/lib -lopencv_core -lopencv_imgproc -lopencv_highgui

# Define the source files
SOURCES=Bildverarbeitung/main.cpp Bildverarbeitung/service.cpp Bildverarbeitung/filtering.cpp Bildverarbeitung/gradient_calculation.cpp

# Define the target binary
TARGET=Bildverarbeitung

# Define the default target (make will run this target if no target is specified)
all: $(TARGET)

# Define the target binary and its dependencies
$(TARGET): $(SOURCES:.cpp=.o)
	$(CC) $(LDFLAGS) $^ -o $@

# Define the dependency rules
%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@

# Clean up the object files and target binary
clean:
	rm -f $(SOURCES:.cpp=.o) $(TARGET)
