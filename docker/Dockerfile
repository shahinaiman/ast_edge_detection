FROM mcr.microsoft.com/dotnet/framework/sdk:4.8

# Install OpenCV
RUN apt-get update && \
    apt-get install -y \
    build-essential \
    cmake \
    pkg-config \
    libjpeg-dev \
    libpng-dev \
    libtiff-dev \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev \
    libv4l-dev \
    libxvidcore-dev \
    libx264-dev \
    libgtk-3-dev \
    libatlas-base-dev \
    gfortran \
    wget \
    unzip \
    apt-get install git \
    apt-get install nano

RUN wget https://github.com/opencv/opencv/archive/4.5.4.zip && \
    unzip 4.5.4.zip && \
    rm 4.5.4.zip && \
    mv opencv-4.5.4 opencv

RUN mkdir /opencv/build && \
    cd /opencv/build && \
    cmake -D CMAKE_BUILD_TYPE=RELEASE \
          -D CMAKE_INSTALL_PREFIX=/usr/local \
          -D INSTALL_C_EXAMPLES=OFF \
          -D INSTALL_PYTHON_EXAMPLES=OFF \
          -D BUILD_EXAMPLES=OFF .. && \
    make -j$(nproc) && \
    make install && \
    ldconfig

# Set environment variables for OpenCV
ENV OpenCV_DIR /usr/local/opencv
ENV OpenCV_INCLUDE_DIR /usr/local/opencv/include
ENV OpenCV_LIB_DIR /usr/local/opencv/lib

# Copy the project files
WORKDIR /app
COPY . /app


# Build the project using MSBuild
RUN msbuild Bildverarbeitung.sln /p:Configuration=Release

# Run the unit tests
RUN vstest.console.exe Bildverarbeitung.Tests/bin/Release/Bildverarbeitung.Tests.dll

apt-get install -y curl

ENV SONAR_USER_HOME /root/.sonar
ENV GIT_DEPTH 0

WORKDIR /root

RUN curl -sSLo sonar-scanner.zip 'https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.7.0.2747-linux.zip' \
    && unzip -o sonar-scanner.zip \
    && rm sonar-scanner.zip \
    && mv sonar-scanner-4.7.0.2747-linux sonar-scanner \
    && curl -sSLo build-wrapper-linux-x86.zip "https://sonarcloud.io/static/cpp/build-wrapper-linux-x86.zip" \
    && unzip -oj build-wrapper-linux-x86.zip -d ./build-wrapper
